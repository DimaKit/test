package kit.test.control;

import java.util.Random;
import java.util.Scanner;

import kit.test.shapes.BasicShape;
import kit.test.shapes.Round;
import kit.test.shapes.Square;
import kit.test.shapes.Trapeze;
import kit.test.shapes.Triangle;

public class Controller {
	private static Random r;
	private static BasicShape bs;

	public static void main(String[] args) {
		r = new Random();
		BasicShape[] shapes = new BasicShape[getShapesQuantity()];
		
		for(int i = 0; i < shapes.length; i++) {
			shapes[i] = getRandomShape();
			System.out.println(shapes[i].convertShapeToString());
		}
	}

	private static BasicShape getRandomShape() {
		int n = r.nextInt(4);
		
		switch (n) {
			case 0:
				bs = new Square();
				break;
			case 1:
				bs = new Round();
				break;
			case 2:
				bs = new Triangle();
				break;
			case 3:
				bs = new Trapeze();
				break;
			default:
				bs = null;
				break;
		}
		
		return bs;
	}

	private static int getShapesQuantity() {
		System.out.println("������ ����� ������ ��������?");
		
		Scanner s = new Scanner(System.in);
		int num = s.nextInt();
		s.close();
		
		return num;
	}
}
