package kit.test.shapes;

public class Triangle extends BasicShape {
	private double cathetus1, cathetus2, hypotenuse;
	
	public Triangle() {
		super();
		cathetus1  = getDouble();
		cathetus2  = getDouble();
		hypotenuse = round(Math.sqrt(cathetus1 * cathetus1 + cathetus2 * cathetus2));
	}
	
	@Override
	public void paint() {
		System.out.println("Painting triangle");
	}

	@Override
	public double getSquare() {
		return round(cathetus1 * cathetus2 / 2);
	}

	@Override
	public String convertShapeToString() {
		return "Գ����: ���������, �����: " + getSquare() + " ��. ��., ������� ���������: " + hypotenuse
				+ " ��., " + getColor();
	}

	public double getHypotenuse() {
		return hypotenuse;
	}

	public double getCathetus1() {
		return cathetus1;
	}

	public double getCathetus2() {
		return cathetus2;
	}
}
