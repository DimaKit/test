package kit.test.shapes;

public class Round extends BasicShape {
	private double radius;
	
	public Round() {
		super();
		radius = getDouble();
	}
	
	@Override
	public void paint() {
		System.out.println("Painting round");
	}

	@Override
	public double getSquare() {
		return round(Math.PI * radius * radius);
	}

	@Override
	public String convertShapeToString() {
		return "Գ����: ����, �����: " + getSquare() + " ��. ��., �����: " + radius
				+ " ��., " + getColor();
	}

	public double getRadius() {
		return radius;
	}
}
