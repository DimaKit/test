package kit.test.shapes;

import java.math.BigDecimal;
import java.util.Random;

public abstract class BasicShape {
	private String color;
	
	private String[] colors = {
			"������", 
			"�����", 
			"����", 
			"��������",
			"������",
			"����"
	};
	
	public BasicShape() {
		color = colors[new Random().nextInt(colors.length)];
	}
	
	public abstract void paint();
	public abstract double getSquare();
	public abstract String convertShapeToString();

	public String getColor() {
		return "����: " + color;
	}
	
	protected double getDouble() {
		return round(new Random().nextDouble() * 20);
	}
	
	protected double round(double n) {
		return BigDecimal.valueOf(n)
				.setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
}
