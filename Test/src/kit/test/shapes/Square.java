package kit.test.shapes;

public class Square extends BasicShape {
	private double sideLength;
	
	public Square() {
		super();
		sideLength = getDouble();
	}
	
	@Override
	public void paint() {
		System.out.println("Painting square");
	}

	@Override
	public double getSquare() {
		return round(sideLength * sideLength);
	}

	@Override
	public String convertShapeToString() {
		return "Գ����: �������, �����: " + getSquare() + " ��. ��., ������� �������: " + sideLength
				+ " ��., " + getColor();
	}

	public double getSideLength() {
		return sideLength;
	}
}
