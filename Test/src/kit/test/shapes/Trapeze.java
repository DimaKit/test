package kit.test.shapes;

public class Trapeze extends BasicShape {
	private double base1, base2, height;
	
	public Trapeze() {
		super();
		base1  = getDouble();
		base2  = getDouble();
		height = getDouble();
	}
	
	@Override
	public void paint() {
		System.out.println("Painting trapeze");
	}

	@Override
	public double getSquare() {
		return round((base1 + base2) / 2 * height);
	}

	@Override
	public String convertShapeToString() {
		return "Գ����: ��������, �����: " + getSquare() + " ��. ��., ������� ������: " + height
				+ " ��., " + getColor();
	}

	public double getBase1() {
		return base1;
	}

	public double getBase2() {
		return base2;
	}

	public double getHeight() {
		return height;
	}
}
